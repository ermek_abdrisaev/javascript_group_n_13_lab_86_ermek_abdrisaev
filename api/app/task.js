const express = require('express');
const path = require('path');
const Task = require('../models/Task');
const router = express.Router();

router.get('/', async (req, res, next) =>{
  try{
    const query = {};
    if(req.query.user){
      const taskId = await Task.find({user:{_id: req.query.user}}).populate('user', 'username');
      console.log(taskId);
      return res.send(taskId);
    }
    const tasks = await Task.find(query).populate('user', 'username');

    return res.send(tasks);
  } catch(e){
    next(e);
  }
});

router.post('/', async(req, res, next) =>{
  try{
    if(!req.body.user){
      return res.send(404).send({message: "Fill required fields"});
    }

    const taskData = {
      _id: req.body._id,
      user: req.body.user,
      title: req.body.title,
    };

    const task = new Task(taskData);
    await task.save();
    return res.send(
      {message: 'Created new task', id: task._id});
  } catch(e){
    next(e);
  }
});

router.put('/:id', async(req, res, next) =>{
 try{
   if (req.body.user){
     const task = await Task.updateOne({_id: req.params.id}, {user: req.body.user});
     return res.send({message: 'Updated your ' + task});
   }
   if (req.body.status){
     const task = await Task.updateOne({_id: req.params.id}, {status: req.body.status});
     return res.send({message: 'Updated your ' + task});
   }

 } catch (e) {
   next(e);
 }
});

router.delete('/:id', async (req, res, next)=>{
  try{
    const task = await Task.deleteOne({_id: req.params.id});
    return res.send(task);
  } catch (e) {
    next(e);
  }

})

module.exports = router;