const path = require('path');
const rootPath = __dirname;

module.exports = {
  rootPath,
  mongo: {
    db: 'mongodb://localhost/todo-list',
    options: {useNewUrlParser: true},
  }
};