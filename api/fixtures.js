const mongoose = require('mongoose');
const config = require('./config');
const Task = require('./models/Task');
const User = require('./models/User');

const run = async () => {
  await mongoose.connect(config.mongo.db, config.mongo.options);
  const collections = await mongoose.connection.db.listCollections().toArray();

  for(const coll of collections){
    await mongoose.connection.db.dropCollection(coll.name);
  }

  const [john, will, tony] = await User.create({
    username: 'John'
  }, {
    username: 'Will'
  }, {
    username: 'Tony'
  })

  await Task.create({
    user: john,
    title: 'Go to sleep',
    status: 'done'
  }, {
    user: will,
    title: 'Go eat something',
    status: 'in_progress'
  }, {
    user: tony,
    title: 'Go walk',
    status: 'new',
  });

  await mongoose.connection.close();

};
run().catch(e => console.error(e));
