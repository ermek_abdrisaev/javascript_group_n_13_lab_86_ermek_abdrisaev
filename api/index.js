const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const task = require('./app/task');
const user = require('./app/user');
const config = require('./config');
const app = express();

const port = 8080;

app.use(cors({origin: 'http://localhost:4200'}));
app.use(express.json());
app.use('/task', task);
app.use('/user', user);

const run = async () =>{
await mongoose.connect(config.mongo.db, config.mongo.options);

app.listen(port, () =>{
  console.log(`Server started on ${port} post!`);
});

process.on('exit', () =>{
  mongoose.disconnect();
});
};

run().catch(e => console.error(e));