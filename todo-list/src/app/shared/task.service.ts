import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { map } from 'rxjs/operators';
import { ApiTaskData, EditStatus, EditUser, Task, TaskData } from '../models/task.model';

@Injectable({
  providedIn: 'root'
})

export class TaskService {
  constructor(private http: HttpClient) {
  };

  getTasks() {
    return this.http.get<ApiTaskData[]>(environment.apiUrl + '/task').pipe(
      map(response => {
        console.log(response);
        return response.map(taskData => {
          return new Task(
            taskData._id,
            taskData.user,
            taskData.title,
            taskData.status,
          );
        });
      })
    );
  }

  getTask(id: string) {
    return this.http.get<ApiTaskData[]>(environment.apiUrl + `/task?=${id}`).pipe(
      map(result => {
        return result.map(taskData => {
          return new Task(taskData._id, taskData.user, taskData.title, taskData.status);
        });
      })
    );
  }

  createTask(taskData: TaskData) {
    return this.http.post(environment.apiUrl + '/task', taskData);
  }

  editTask(editTask: EditUser | EditStatus) {
    return this.http.put(environment.apiUrl + `/task/${editTask._id}`, editTask);
  }

  removeTask(id: string) {
    return this.http.delete(environment.apiUrl + `/task/${id}`);
  }
}
