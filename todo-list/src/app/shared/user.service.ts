import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiUserData, User, UserData } from '../models/user.model';
import { environment } from '../../environments/environment';
import { map } from 'rxjs/operators';
import { EditStatus, EditUser } from '../models/task.model';

@Injectable({
  providedIn: 'root'
})

export class UserService {
  constructor(private http: HttpClient) {
  }

  getUsers() {
    return this.http.get<ApiUserData[]>(environment.apiUrl + '/user').pipe(
      map(response => {
        return response.map(userData => {
          return new User(
            userData._id,
            userData.username,
          );
        });
      })
    );
  }

  getUser(id: string) {
    return this.http.get<ApiUserData[]>(environment.apiUrl + `/user?=${id}`).pipe(
      map(result => {
        return result.map(userData => {
          return new User(userData._id, userData.username);
        });
      })
    );
  }

  createUser(userData: UserData) {
    return this.http.post(environment.apiUrl + 'user', userData);
  }

  editUser(editUser: EditUser | EditStatus) {
    return this.http.put(environment.apiUrl + `/user/${editUser._id}`, editUser);
  }

  removeUser(id: string) {
    return this.http.delete(environment.apiUrl + `/user/${id}`);
  }
}
