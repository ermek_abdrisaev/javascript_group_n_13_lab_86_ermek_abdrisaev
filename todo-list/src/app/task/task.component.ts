import { Component, OnInit } from '@angular/core';
import { AppState } from '../store/types';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { deleteTaskRequest, editTaskRequest, fetchTasksRequest } from '../store/task.action';
import { Task } from '../models/task.model';
import { User } from '../models/user.model';
import { fetchUsersRequest } from '../store/user.actions';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.sass']
})

export class TaskComponent implements OnInit {
  task: Observable<Task[]>;
  user!: Observable<User[]>;
  loading: Observable<boolean>;
  error: Observable<null | string>;


  constructor(private store: Store<AppState>, private route: ActivatedRoute) {
    this.task = store.select(state => state.task.task);
    this.loading = store.select(state => state.task.fetchLoading);
    this.error = store.select(state => state.task.fetchError);

    this.user = store.select(state => state.user.user);
    this.loading = store.select(state => state.user.fetchLoading);
    this.error = store.select(state => state.user.fetchError);
  }

  ngOnInit(): void {
    this.store.dispatch(fetchTasksRequest());
    this.store.dispatch(fetchUsersRequest());
  }

  onEditUser(id: string, event: Event) {
    const data = {
      _id: id,
      user: (<HTMLSelectElement>event.target).value
    };
    this.store.dispatch(editTaskRequest({editTask: data}));
  }

  onEditStatus(id: string, event: Event) {
    const data = {
      _id: id,
      status: (<HTMLSelectElement>event.target).value
    };
    this.store.dispatch(editTaskRequest({editTask: data}));
  }

  onDelete(id: string) {
    this.store.dispatch(deleteTaskRequest({id}));
  }
}
