import { Observable } from 'rxjs';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Store } from '@ngrx/store';
import { Task, TaskData } from '../../models/task.model';
import { AppState } from '../../store/types';
import { ActivatedRoute } from '@angular/router';
import { createTaskRequest } from '../../store/task.action';
import { User } from '../../models/user.model';
import { NgForm } from '@angular/forms';
import { fetchUsersRequest } from '../../store/user.actions';

@Component({
  selector: 'app-new-task',
  templateUrl: './new-task.component.html',
  styleUrls: ['./new-task.component.sass']
})
export class NewTaskComponent implements OnInit {
  @ViewChild('f') form!: NgForm;
  selected = 'None';
  task: Observable<Task[]>;
  user: Observable<User[]>;
  loading: Observable<boolean>;
  error: Observable<null | string>;
  name!: string;

  constructor(private store: Store<AppState>, private route: ActivatedRoute) {
    this.task = store.select(state => state.task.task);
    this.loading = store.select(state => state.task.fetchLoading);
    this.error = store.select(state => state.task.fetchError);
    this.user = store.select(state => state.user.user);
  }

  ngOnInit(): void {
    this.store.dispatch(fetchUsersRequest());
  }

  onSubmit() {
    const taskData: TaskData = {
      title: this.form.value.title,
      user: this.form.value.user
    };
    this.store.dispatch(createTaskRequest({taskData}));
  }
}
