import { User } from '../models/user.model';
import { Task } from '../models/task.model';

export type TasksState = {
  task: Task[];
  fetchLoading: boolean,
  fetchError: null | string,
  createLoading: boolean,
  createError: null | string,
};

export type UserState = {
  user: User[],
  fetchLoading: boolean,
  fetchError: null | string,
  createLoading: boolean,
  createError: null | string,
};

export type AppState = {
  task: TasksState,
  user: UserState
}
