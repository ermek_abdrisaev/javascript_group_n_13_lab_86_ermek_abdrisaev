import { createAction, props } from '@ngrx/store';
import { UserData } from '../models/user.model';

export const fetchUsersRequest = createAction('[User] Fetch Request');
export const fetchUsersSuccess = createAction('[User] Fetch Success', props<{ user: UserData[] }>());
export const fetchUsersFailure = createAction('[User] Fetch Failure', props<{ error: string }>());

export const createUserRequest = createAction('[User] Create Request', props<{ userData: UserData }>());
export const createUserSuccess = createAction('[User] Create Success');
export const createUserFailure = createAction('[User} Create Failure', props<{ error: string }>());

