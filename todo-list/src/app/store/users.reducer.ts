import { createReducer, on } from '@ngrx/store';
import { UserState } from './types';
import {
  createUserFailure,
  createUserRequest,
  createUserSuccess,
  fetchUsersFailure,
  fetchUsersRequest,
  fetchUsersSuccess
} from './user.actions';

const initialState: UserState = {
  user: [],
  fetchLoading: false,
  fetchError: null,
  createLoading: false,
  createError: null,
};

export const userReducer = createReducer(
  initialState,
  on(fetchUsersRequest, state => ({...state, fetchLoading: true})),
  on(fetchUsersSuccess, (state, {user}) => ({...state, fetchLoading: false, user})),
  on(fetchUsersFailure, (state, {error}) => ({...state, fetchLoading: false, fetchError: error})),

  on(createUserRequest, state => ({...state, createLoading: true})),
  on(createUserSuccess, state => ({...state, createLoading: false})),
  on(createUserFailure, (state, {error}) => ({...state, createLoading: false, createError: error}))
);
