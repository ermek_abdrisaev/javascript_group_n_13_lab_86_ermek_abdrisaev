import { createAction, props } from '@ngrx/store';
import { EditStatus, EditUser, Task, TaskData } from '../models/task.model';

export const fetchTasksRequest = createAction('[Task] Fetch Request');
export const fetchTasksSuccess = createAction('[Task] Fetch success', props<{ task: Task[] }>());
export const fetchTasksFailure = createAction('[Task] Fetch failure', props<{ error: string }>());

export const createTaskRequest = createAction('[Task] Create Request', props<{ taskData: TaskData }>());
export const createTaskSuccess = createAction('[Task] Create Success');
export const createTaskFailure = createAction('[Task] Create Failure', props<{ error: string }>());

export const editTaskRequest = createAction('[Task] Edit Request', props<{ editTask: EditUser | EditStatus }>());
export const editTaskSuccess = createAction('[Task] Edit Success');
export const editTaskFailure = createAction('[Task] Edit Failure', props<{ error: string }>());

export const deleteTaskRequest = createAction('[Task] Delete Request', props<{ id: string }>());
export const deleteTaskSuccess = createAction('[Task] Delete Success');
export const deleteTaskFailure = createAction('[Task] Delete Failure', props<{ error: string }>());
