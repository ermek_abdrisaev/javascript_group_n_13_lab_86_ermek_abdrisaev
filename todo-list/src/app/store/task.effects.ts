import { Injectable } from '@angular/core';
import { TaskService } from '../shared/task.service';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import {
  createTaskFailure,
  createTaskRequest,
  createTaskSuccess,
  deleteTaskFailure,
  deleteTaskRequest,
  deleteTaskSuccess,
  editTaskFailure,
  editTaskRequest,
  editTaskSuccess,
  fetchTasksFailure,
  fetchTasksRequest,
  fetchTasksSuccess
} from './task.action';
import { catchError, mergeMap, of, tap } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable()
export class TaskEffects {
  fetchTasks = createEffect(() => this.actions.pipe(
    ofType(fetchTasksRequest),
    mergeMap(() => this.taskService.getTasks().pipe(
      map(task => fetchTasksSuccess({task})),
      catchError(() => of(fetchTasksFailure({error: 'Something goes wrong'})))
    ))
  ));

  createTask = createEffect(() => this.actions.pipe(
  ofType(createTaskRequest),
    mergeMap(({taskData}) => this.taskService.createTask(taskData).pipe(
      map( () => createTaskSuccess()),
        tap(() => this.router.navigate(['/task'])),
        catchError(() => of(createTaskFailure({error: 'Wrong data'})))
      ))
    ));

  editTask = createEffect(() => this.actions.pipe(
    ofType(editTaskRequest),
    mergeMap(({editTask}) => this.taskService.editTask(editTask).pipe(
      map(() => editTaskSuccess()),
      catchError(() => of(editTaskFailure({error: 'Wrong command'})))
    ))
  ));

  deleteTask = createEffect(() => this.actions.pipe(
    ofType(deleteTaskRequest),
    mergeMap(({id}) => this.taskService.removeTask(id).pipe(
      map( () => deleteTaskSuccess()),
      catchError(() => of(deleteTaskFailure({error: 'Not deleted'})))
    ))
  ));

  constructor(
    private actions: Actions,
    private taskService: TaskService,
    private router: Router
  ) {
  }
}
