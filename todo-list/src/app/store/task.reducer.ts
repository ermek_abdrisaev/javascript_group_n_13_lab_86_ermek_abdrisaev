import { createReducer, on } from '@ngrx/store';
import {
  createTaskFailure,
  createTaskRequest,
  createTaskSuccess,
  deleteTaskFailure,
  deleteTaskRequest,
  deleteTaskSuccess,
  editTaskFailure,
  editTaskRequest,
  editTaskSuccess,
  fetchTasksFailure,
  fetchTasksRequest,
  fetchTasksSuccess
} from './task.action';
import { TasksState } from './types';

const initialState: TasksState = {
  task: [],
  fetchLoading: false,
  fetchError: null,
  createLoading: false,
  createError: null
};

export const taskReducer = createReducer(
  initialState,
  on(fetchTasksRequest, state => ({...state, fetchLoading: true})),
  on(fetchTasksSuccess, (state, {task}) => ({...state, fetchLoading: false, task})),
  on(fetchTasksFailure, (state, {error}) => ({...state, fetchLoading: false, fetchError: error})),

  on(createTaskRequest, state => ({...state, createLoading: true})),
  on(createTaskSuccess, state => ({...state, createLoading: false})),
  on(createTaskFailure, (state, {error}) => ({...state, createLoading: false, createError: error})),

  on(editTaskRequest, state => ({...state, createLoading: true})),
  on(editTaskSuccess, state => ({...state, createLoading: false})),
  on(editTaskFailure, (state, {error}) => ({...state, createLoading: false, createError: error})),

  on(deleteTaskRequest, state => ({...state, createLoading: true})),
  on(deleteTaskSuccess, state => ({...state, createLoading: false})),
  on(deleteTaskFailure, (state, {error}) => ({...state, createLoading: false, createError: error})),
);
