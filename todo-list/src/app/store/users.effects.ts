import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { UserService } from '../shared/user.service';
import {
  createUserFailure,
  createUserRequest,
  createUserSuccess,
  fetchUsersFailure,
  fetchUsersRequest,
  fetchUsersSuccess
} from './user.actions';
import { catchError, mergeMap, of } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class UsersEffects {
  fetchUsers = createEffect(() => this.actions.pipe(
    ofType(fetchUsersRequest),
    mergeMap(() => this.userService.getUsers().pipe(
      map(user => fetchUsersSuccess({user})),
      catchError(() => of(fetchUsersFailure({error: 'Something goes wrong'})))
    ))
  ));

  createUser = createEffect(() => this.actions.pipe(
    ofType(createUserRequest),
    mergeMap(({userData}) => this.userService.createUser(userData).pipe(
      map(() => createUserSuccess()),
      catchError(() => of(createUserFailure({error: 'Wrong data'})))
    ))
  ));

  constructor(
    private actions: Actions,
    private userService: UserService,
  ) {
  }
}
